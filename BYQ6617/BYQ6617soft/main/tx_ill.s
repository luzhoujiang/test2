;/**************************************************************************/ 
;/*                                                                        */ 
;/*            Copyright (c) 1996-1999 by Express Logic Inc.               */ 
;/*                                                                        */ 
;/*  This software is copyrighted by and is the sole property of Express   */ 
;/*  Logic, Inc.  All rights, title, ownership, or other interests         */ 
;/*  in the software remain the property of Express Logic, Inc.  This      */ 
;/*  software may only be used in accordance with the corresponding        */ 
;/*  license agreement.  Any unauthorized use, duplication, transmission,  */ 
;/*  distribution, or disclosure of this software is expressly forbidden.  */ 
;/*                                                                        */
;/*  This Copyright notice may not be removed or modified without prior    */ 
;/*  written consent of Express Logic, Inc.                                */ 
;/*                                                                        */ 
;/*  Express Logic, Inc. reserves the right to modify this software        */ 
;/*  without notice.                                                       */ 
;/*                                                                        */ 
;/*  Express Logic, Inc.                                                   */
;/*  11440 West Bernardo Court               info@expresslogic.com         */
;/*  Suite 300                               http://www.expresslogic.com   */
;/*  San Diego, CA  92127                                                  */
;/*                                                                        */
;/**************************************************************************/
;
;
;/**************************************************************************/
;/**************************************************************************/
;/**                                                                       */ 
;/** ThreadX Component                                                     */ 
;/**                                                                       */
;/**   Initialize (INI)                                                    */
;/**                                                                       */
;/**************************************************************************/
;/**************************************************************************/
;
;
;#define    TX_SOURCE_CODE
;
;
;/* Include necessary system files.  */
;
;#include   "tx_api.h"
;#include   "tx_ini.h"
;#include   "tx_thr.h"
;#include   "tx_tim.h"
   
RAM_BASE        EQU     0x0                 ;
RAM_BASE_BOOT   EQU     0x300000            ;


SVC_MODE        EQU     0xD3                ; Disable irq,fiq SVC mode
IRQ_MODE        EQU     0xD2                ; Disable irq,fiq IRQ mode
FIQ_MODE        EQU     0xD1                ; Disable irq,fiq FIQ mode
;
EBI_CSR0        EQU     0xFFE00000
EBI_CSR1        EQU     0xFFE00004
EBI_CSR2        EQU     0xFFE00008
EBI_CSR3        EQU     0xFFE0000C
EBI_RCR         EQU     0xFFE00020
EBI_MCR         EQU     0xFFE00024

TC_CHAN2        EQU     0xFFFE0080          ; TC Channel3 Address;
TC_BCR          EQU     0xFFFE00C0          ; TC Block Control Register;
TC_BMR          EQU     0xFFFE00C4          ; TC Block Mode Register;
TC_CCR          EQU     0x00                ; Channel Control Register
TC_CMR          EQU     0x04                ; Channel Mode Register 
TC_RC           EQU     0x1C                ; Register C
TC_SR           EQU     0x20                ; Status  Register
TC_IER          EQU     0x24                ; Interrupt Enable Register
TC_IDR          EQU     0x28                ; Interrupt Disable Register

PS_PCER         EQU     0xFFFF4004          ; PS Enable Register

AIC_BASE        EQU     0xFFFFF000          ;
AIC_SVR         EQU     0x080               ;
AIC_SMR6        EQU     0xFFFFF018          ; AIC source Mode Register 7
AIC_IVR         EQU     0xFFFFF100          ; The Interrupt Vector Register;
AIC_IECR        EQU     0xFFFFF120          ; Interrupt Enable Command Register;
AIC_IDCR        EQU     0xFFFFF124          ; Interrupt Disable Command Register;
AIC_ICCR        EQU     0xFFFFF128          ; Interrupt Clear Command Register;
AIC_ISCR        EQU     0xFFFFF12C          ; Interrupt Set Command Register;
AIC_EOICR       EQU     0xFFFFF130          ; End of Interrupt Command Register;
AIC_SPU         EQU     0xFFFFF134          ;

TIME_VALUE      EQU     0xff
ALL_INT         EQU     0x701FF

    IMPORT      _tx_thread_system_stack_ptr
    IMPORT      _tx_initialize_unused_memory
    IMPORT      _tx_thread_context_save
    IMPORT      _tx_thread_context_restore
    IMPORT      _tx_timer_interrupt
    IMPORT      _tx_timer_stack_start
    IMPORT      _tx_timer_stack_size
    IMPORT      _tx_timer_priority
    IMPORT      main
    IMPORT      |Image$$RO$$Limit|
    IMPORT      |Image$$RW$$Base|
    IMPORT      |Image$$ZI$$Base|
    IMPORT      |Image$$ZI$$Limit|

; 
;
        AREA  Init, CODE, READONLY
;
;/* Define the ARM7 vector area.  This should be located or copied to 0.  */
;
    EXPORT  __vectors
__vectors
    B       __main                          ; Reset goes to startup function
    B       __tx_undefined                  ; Undefined handler
    B       __tx_swi_interrupt              ; Software interrupt handler
    B       __tx_prefetch_handler           ; Prefetch exeception handler
    B       __tx_abort_handler              ; Abort exception handler
    B       __tx_reserved_handler           ; Reserved exception handler
    B       __tx_irq_handler                ; IRQ interrupt handler
    B       __tx_fiq_handler                ; FIQ interrupt handler
    
VectorTable
    ldr         pc, [pc, #&18]          ; SoftReset
    ldr         pc, [pc, #&18]          ; UndefHandler
    ldr         pc, [pc, #&18]          ; SWIHandler
    ldr         pc, [pc, #&18]          ; PrefetchAbortHandler
    ldr         pc, [pc, #&18]          ; DataAbortHandler
    nop                                 ; Reserved
    ldr         pc, [pc,#-&F20]         ; IRQ : read the AIC
    ldr         pc, [pc,#-0xF20]        ; FIQ : read the AIC
    
;/*- There are only 5 offsets as the vectoring is used.*/
    DCD         __main 
    DCD         __tx_undefined
    DCD         __tx_swi_interrupt
    DCD         __tx_prefetch_handler
    DCD         __tx_abort_handler



;
;
;/**************************************************************************/
;/*                                                                        */
;/*  FUNCTION                                               RELEASE        */
;/*                                                                        */
;/*    __main                                               ARM7/ARM       */
;/*                                                           3.0e         */ 
;/*  AUTHOR                                                                */ 
;/*                                                                        */ 
;/*    William E. Lamie, Express Logic, Inc.                               */ 
;/*                                                                        */ 
;/*  DESCRIPTION                                                           */ 
;/*                                                                        */ 
;/*    This function is the entry function for the ARM compiler tools.     */ 
;/*    It is responsible for setting up the initialized data area,         */ 
;/*    non-initiailzed data area, and the initial stack pointers.  When    */ 
;/*    processing is finished, the routine jumps to the user's main        */ 
;/*    function.                                                           */ 
;/*                                                                        */ 
;/*  INPUT                                                                 */ 
;/*                                                                        */ 
;/*    None                                                                */ 
;/*                                                                        */ 
;/*  OUTPUT                                                                */ 
;/*                                                                        */ 
;/*    None                                                                */ 
;/*                                                                        */ 
;/*  CALLS                                                                 */ 
;/*                                                                        */ 
;/*    None                                                                */ 
;/*                                                                        */ 
;/*  CALLED BY                                                             */ 
;/*                                                                        */ 
;/*    Reset Vectors                                                       */ 
;/*                                                                        */ 
;/*  RELEASE HISTORY                                                       */ 
;/*                                                                        */ 
;/*    DATE              NAME                      DESCRIPTION             */ 
;/*                                                                        */ 
;/*  06-15-1997     William E. Lamie         Initial Version 3.0           */ 
;/*  11-11-1997     William E. Lamie         Modified comment(s),          */ 
;/*                                            resulting in version 3.0b.  */ 
;/*  03-01-1998     William E. Lamie         Modified comment(s),          */ 
;/*                                            resulting in version 3.0d.  */ 
;/*  01-01-1999     William E. Lamie         Modified comment(s) and fixed */ 
;/*                                            ROM copy loop compare,      */ 
;/*                                            resulting in version 3.0e.  */ 
;/*                                                                        */ 
;/**************************************************************************/ 
    EXPORT __entryX
__entryX
    EXPORT __main
__main
    ENTRY
;
    MOV     a1, #SVC_MODE                   ; Build interrupt lockout, SVC mode
    MSR     CPSR_c, a1                        ; Ensure ints locked out and SVC mode
    
;
;    /****** NOTE ****** We must be in SVC MODE at this point.  Some monitors
;       enter this routine in USER mode and require a software interrupt to
;       change into SVC mode.  */
;
;    /*- Perform 8 End Of Interrupt Command to make sure AIC will not lock out nIRQ*/
    mov     r0, #8
    mov     r1, #0
LoopAic0 
    str     r1, [r1, #AIC_EOICR]    ; any value written
    subs    r0, r0, #1
    bhi     LoopAic0
      
    add     r0, pc,#-(8+.-AicData)  ; @ where to read values (relative)
    ldmia   r0, {r1-r3}
;/*- Set up the default interrupt handler vectors*/
    str     r2, [r1, #AIC_SVR]      ; SVR[0] for FIQ
    add     r1, r1, #AIC_SVR
    mov     r0, #31                 ; counter
LoopAic1
    str     r3, [r1, r0, LSL #2]    ; SVRs for IRQs
    subs    r0, r0, #1              ; do not save FIQ
    bhi     LoopAic1
    
    ;/*- Copy the ARM exception vectors*/
;/* The RAM_BASE = 0 it's specific for ICE*/
    mov     r8,#RAM_BASE              ; @ of the hard vector after remap  in internal RAM 0x0
    add     r9, pc,#-(8+.-VectorTable); @ where to read values (relative)
    ldmia   r9!, {r0-r7}              ; read 8 vectors
    stmia   r8!, {r0-r7}              ; store them
    ldmia   r9!, {r0-r4}                    ; read 5 absolute handler addresses 
    stmia   r8!, {r0-r4}                    ; store them 
    
    IMPORT      Initial
    BL  Initial
    
;    /* Perform C memory initialization, i.e. setup initialized C variables and
;       clear the uninitialized C variables to 0.  */
;
    LDR     a1, =|Image$$RO$$Limit|         ; Get start of ROM copy of data
    LDR     a2, =|Image$$RW$$Base|          ; Get start of RAM destination
    LDR     a4, =|Image$$ZI$$Base|          ; End of RAM copy destination
    CMP     a1, a2                          ; Check for something to initialize 
    BEQ     __tx_ROM_copy_done              ; If not, skip the copy loop
;
__tx_ROM_copy_loop
    CMP     a2, a4                          ; Check for completion
    LDRCC   a3, [a1], #4                    ; Pickup ROM data
    STRCC   a3, [a2], #4                    ; Store it in RAM area
    BCC     __tx_ROM_copy_loop              ; Continue if more to copy
;
__tx_ROM_copy_done
;
;    /* Clear the Non-Initialized Data area.  */
;
    LDR     a2, =|Image$$ZI$$Limit|         ; Get end of non-initialized RAM area
    MOV     a3, #0                          ; Build clear value
__tx_RAM_clear
    CMP     a4, a2                          ; Check for completion
    STRCC   a3, [a4], #4                    ; Clear non-initialized RAM area
    BCC     __tx_RAM_clear                  ; Continue if more to clear
;
;    /* Setup initial stack pointers for SVC, IRQ, and FIQ modes.  */
;
    
    LDR     a3, [pc, #FIQ_STACK_SIZE-.-8]   ; Pickup stack size
    MOV     a1, #FIQ_MODE                   ; Build FIQ mode CPSR
    MSR     CPSR_c, a1                        ; Enter FIQ mode
    ADD     a2, a2, a3                      ; Calculate start of FIQ stack
    BIC     a2, a2, #3                      ; Ensure long word alignment
    SUB     a2, a2, #4                      ; Backup one word
    MOV     sp, a2                          ; Setup FIQ stack pointer
    MOV     sl, #0                          ; Clear sl
    MOV     fp, #0                          ; Clear fp
    LDR     a3, [pc, #SYS_STACK_SIZE-.-8]   ; Pickup IRQ (system stack size)
    MOV     a1, #IRQ_MODE                   ; Build IRQ mode CPSR
    MSR     CPSR_c, a1                        ; Enter IRQ mode
    ADD     a2, a2, a3                      ; Calculate start of IRQ stack
    BIC     a2, a2, #3                      ; Ensure long word alignment
    SUB     a2, a2, #4                      ; Backup one word
    MOV     sp, a2                          ; Setup IRQ stack pointer
    MOV     a1, #SVC_MODE                   ; Build SVC mode CPSR
    MSR     CPSR_c, a1                        ; Enter SVC mode
    LDR     a4, [pc, #SYS_STACK_PTR-.-8]    ; Pickup stack pointer
    STR     a2, [a4, #0]                    ; Save the system stack
    MOV     sp, a2                          ; Use IRQ stack during initialization
    MOV     fp, #0                          ; Clear frame pointer
    MOV     sl, #0                          ; Clear stack limit
    
;
;    /* Call the C main routine.  */
;
    BL      main                            ; Call main function

exit
    B       exit                        ; Just sit here!

;
;
;/**************************************************************************/ 
;/*                                                                        */ 
;/*  FUNCTION                                               RELEASE        */ 
;/*                                                                        */ 
;/*    _tx_initialize_low_level                             ARM7/ARM       */ 
;/*                                                           3.0e         */ 
;/*  AUTHOR                                                                */ 
;/*                                                                        */ 
;/*    William E. Lamie, Express Logic, Inc.                               */ 
;/*                                                                        */ 
;/*  DESCRIPTION                                                           */ 
;/*                                                                        */ 
;/*    This function is responsible for any low-level processor            */ 
;/*    initialization, including setting up interrupt vectors, saving the  */ 
;/*    system stack pointer, finding the first available memory address,   */ 
;/*    and setting up parameters for the system's timer thread.            */ 
;/*                                                                        */ 
;/*  INPUT                                                                 */ 
;/*                                                                        */ 
;/*    None                                                                */ 
;/*                                                                        */ 
;/*  OUTPUT                                                                */ 
;/*                                                                        */ 
;/*    None                                                                */ 
;/*                                                                        */ 
;/*  CALLS                                                                 */ 
;/*                                                                        */ 
;/*    None                                                                */ 
;/*                                                                        */ 
;/*  CALLED BY                                                             */ 
;/*                                                                        */ 
;/*    _tx_initialize_kernel_enter           ThreadX entry function        */ 
;/*                                                                        */ 
;/*  RELEASE HISTORY                                                       */ 
;/*                                                                        */ 
;/*    DATE              NAME                      DESCRIPTION             */ 
;/*                                                                        */ 
;/*  06-15-1997     William E. Lamie         Initial Version 3.0           */ 
;/*  11-11-1997     William E. Lamie         Modified comment(s),          */ 
;/*                                            resulting in version 3.0b.  */ 
;/*  03-01-1998     William E. Lamie         Modified comment(s),          */ 
;/*                                            resulting in version 3.0d.  */ 
;/*  01-01-1999     William E. Lamie         Modified comment(s),          */ 
;/*                                            resulting in version 3.0e.  */ 
;/*                                                                        */ 
;/**************************************************************************/ 
;VOID   _tx_initialize_low_level(VOID)
;{
    EXPORT  _tx_initialize_low_level
_tx_initialize_low_level
;
;    /* Save the system stack pointer.  */
;    _tx_thread_system_stack_ptr = (VOID_PTR) (sp);
;
    LDR     a2, [pc, #SYS_STACK_PTR-.-8]    ; Pickup address of system stack ptr
    LDR     a1, [a2, #0]                    ; Pickup system stack 
    ADD     a1, a1, #4                      ; Increment to next free word
;
;    /* Pickup the first available memory address.  */
;
;    /* Allocate space for the timer thread's stack.  */
;    _tx_timer_stack_start =        first_available_memory;
;    _tx_timer_stack_size =         stack_size;
;    _tx_timer_priority =           0;
;
    LDR     a2, [pc, #TIMER_STACK-.-8]      ; Pickup timer stack ptr address
    LDR     a4, [pc, #TIMER_STACK_SIZE-.-8] ; Pickup timer stack size address
    LDR     a3, [pc, #TIM_STACK_SIZE-.-8]   ; Pickup actual stack size
    STR     a1, [a2, #0]                    ; Store timer stack base
    STR     a3, [a4, #0]                    ; Store timer stack size
    ADD     a1, a1, a3                      ; New free memory address
    LDR     a2, [pc, #TIMER_PRIORITY-.-8]   ; Pickup timer priority address
    MOV     a3, #0                          ; Build timer thread priority
    STR     a3, [a2, #0]                    ; Store timer thread priority
;
;    /* Save the first available memory address.  */
;    _tx_initialize_unused_memory =  (VOID_PTR) System Stack + Timer Stack;
;
    LDR     a3, [pc, #UNUSED_MEMORY-.-8]    ; Pickup unused memory ptr address
    STR     a1, [a3, #0]                    ; Save first free memory address

    
;/* Enable TC2 Interrupt */
    LDR r0,=AIC_IECR
    LDR r2,=0x40
    STR r2,[r0,#0]
    
    LDR r0,=AIC_EOICR
    STR r1,[r0]

;
;    /* Done, return to caller.  */
;
    MOV     pc, lr                          ; Return to caller
;}
;
;
;/* Define shells for each of the interrupt vectors.  */
;
    EXPORT  __tx_undefined
__tx_undefined
;    B       __tx_undefined                  ; Undefined handler
    ldr         pc, =0x10000dc
;
    EXPORT  __tx_swi_interrupt
__tx_swi_interrupt
;    B       __tx_swi_interrupt              ; Software interrupt handler
    ldr         pc, =0x10000dc
;
    EXPORT  __tx_prefetch_handler
__tx_prefetch_handler
;    B       __tx_prefetch_handler           ; Prefetch exeception handler
    ldr         pc, =0x10000dc
;
    EXPORT  __tx_abort_handler
__tx_abort_handler
;    B       __tx_abort_handler              ; Abort exception handler
    ldr         pc, =0x10000Dc
;
    EXPORT  __tx_reserved_handler
__tx_reserved_handler
;    B       __tx_reserved_handler           ; Reserved exception handler
    ldr         pc, =0x10000dc
;
    EXPORT  __tx_irq_handler
    EXPORT  __tx_irq_processing_return      
__tx_irq_handler
;
;    /* Jump to context save to save system context.  */
    B       _tx_thread_context_save 
__tx_irq_processing_return
;
;    /* At this point execution is still in the IRQ mode.  The CPSR, point of
;       interrupt, and all C scratch registers are available for use.  In 
;       addition, IRQ interrupts may be re-enabled if nested IRQ interrupts
;       are desired.  */
;
;    /* For debug purpose, execute the timer interrupt processing here.  In
;       a real system, some kind of status indication would have to be checked
;       before the timer interrupt handler could be called.  */

    IMPORT      irq_handler
    BL  irq_handler 


;    BL      _tx_timer_interrupt             ; Timer interrupt handler
    
;
;    /* Jump to context restore to restore system context.  */
    B       _tx_thread_context_restore
;


    EXPORT  __tx_fiq_handler
__tx_fiq_handler
    B       __tx_fiq_handler                ; FIQ interrupt handler
;
;
SYS_STACK_PTR
    DCD     _tx_thread_system_stack_ptr
FIQ_STACK_SIZE
    DCD     512                             ; Keep 4-byte alignment
SYS_STACK_SIZE
    DCD     1024                            ; Keep 4-byte alignment
TIM_STACK_SIZE
    DCD     1024                            ; Timer stack size
UNUSED_MEMORY
    DCD     _tx_initialize_unused_memory
TIMER_STACK
    DCD     _tx_timer_stack_start
TIMER_STACK_SIZE
    DCD     _tx_timer_stack_size
TIMER_PRIORITY	
    DCD     _tx_timer_priority

;/*- Default Interrupt Handlers*/
;;    IMPORT  spurious_handler
AicData
    DCD     AIC_BASE                ; AIC Base Address
PtDefaultHandler
    DCD     __tx_fiq_handler
    DCD     __tx_irq_handler
;;AicSpu
;;    DCD     spurious_handler
    
    END

